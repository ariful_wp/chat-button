
const chatBox = document.querySelector(".chat-box");
const btnChat = document.querySelector(".msg-btn");
const btnClose = document.querySelector(".msg-close");

btnChat.onclick = () => {
  chatBox.classList.add("show");
}
btnClose.onclick = () => {
  chatBox.classList.remove("show");
}